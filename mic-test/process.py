#!/bin/env python3
import serial
import sys
#import matplotlib.pyplot as plt
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
#import re
import numpy as np
from scipy.signal import get_window
import time

NUM_SAMPLES = 1024
header = b'\x00\xff\x00\xff\x00\xff\x00\xff'
#pattern = re.compile("{}(.{{{}}})".format(header, NUM_SAMPLES * 12 // 8).encode())

class Plotter():

    def __init__(self):
        self.app = QtGui.QApplication([])
        self.plot = pg.plot(title='Plot')
        self.plot.setLabel('left', 'level', 'dB')
        self.plot.setLabel('bottom', 'Frequency', '')
        self.x = np.linspace(0, 6e3, NUM_SAMPLES)
        self.y = np.zeros(NUM_SAMPLES)
        self.curve = self.plot.plot(self.x, self.y)
        self.buffer = b""
        self.spectrum = np.zeros(NUM_SAMPLES // 2 + 1)
        self.count = 0

        self.ser = ser = serial.Serial('/dev/ttyUSB0', 576000, timeout=0)
        # discard whatever might be in the buffer
        ser.flushInput()
        ser.flushOutput()

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.refresh)
        self.timer.start(100)




    def refresh(self, averages=5):
        # add get spectrums untill there are none to add
        print("refreshing...")
        while self.count < averages:
            f = self.get_spectrum()
            if f is None:
                print("not enough")
                return None
            self.spectrum = self.spectrum + f
            self.count += 1

        print("averages acquired")
        # update plot
        self.x = np.linspace(0, 100/92*6e3, NUM_SAMPLES // 2 + 1)
        self.y = self.spectrum / averages
        self.curve.setData(self.x, self.y)
        self.app.processEvents()

        # clear averages
        self.count = 0
        self.spectrum = np.zeros(NUM_SAMPLES // 2 + 1)

    def get_spectrum(self):
        raw = self.get_trace()
        if raw is None:
            return None
        samples = self.unpack(raw)
        #print(samples)

        window = get_window('blackmanharris', len(samples))

        f = np.fft.rfft(samples * window)
        f = f * f.conj()
        f = f.real
        f = 10. * np.log10(f)
        return f


    last = time.time()
    def get_trace(self):
        self.buffer += self.ser.read_all() or b""

        # find the first header
        first = self.buffer.find(header)
        if first < 0:
            return None

        # find the second header
        second = self.buffer.find(header, first + 1)
        if second < 0:
            self.buffer = self.buffer[first:]
            return None

        dt = time.time() - self.last
        print(dt)
        self.last = time.time()
        # cut the chuck we want:
        data = self.buffer[first+len(header):second]

        # truncate buffer
        self.buffer = self.buffer[second:]

        # return data
        return list(data)



    def get_last_trace(self):
        self.buffer += self.ser.read_all() or b""

        # the packet we want is between the last and the semilast header
        # in any case we discard everything before the last header

        # first find the last header
        last = self.buffer.rfind(header)
        if last < 0:
            return None

        # find the second to last header
        penultimate = self.buffer.rfind(header, 0, last)
        if penultimate < 0:
            self.buffer = self.buffer[last:]
            return None

        # cut the chuck we want:
        data = self.buffer[penultimate+len(header):last]

        # truncate buffer
        self.buffer = self.buffer[last:]

        # return data
        return list(data)

    def unpack(self, data):
        # extract tightly packed bytes
        d = np.array(data).reshape([-1, 3]) # pairs of 2
        d1 = (d[:, 0] << 4) | ((d[:,1] & 0xF0) >> 4)
        d2 = ((d[:, 1] & 0x0F) << 8) | (d[:,2] & 0xFF)
        return np.array([d1, d2]).transpose().reshape([-1])


    def run(self):
        self.app.exec_()


if __name__=="__main__":
    m = Plotter()
    m.run()
