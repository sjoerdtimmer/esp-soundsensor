#include <Arduino.h>

#define NUM_SAMPLES 1024
#define MIC_PIN 34 // pin 10, ADC1_6
#define DELAY_US 22 // ~44.1kHz

uint8_t header[] = {0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF};
uint16_t samples[NUM_SAMPLES];
uint8_t output_buffer[NUM_SAMPLES*12/8];


void setup() {
  Serial.begin(576000);
  //Serial.println("Hello World!");
}

void loop() {
  // Serial.println("loop");
  // get samples
  for (int i=0; i<NUM_SAMPLES; i++) {
    uint16_t val = analogRead(MIC_PIN);
    //val = i;
    //samples[i] = analogRead(MIC_PIN);
    // reading takes more time anyway
    delayMicroseconds(DELAY_US);
    if (i%2 == 0) {
      // even samples have their 8 most significant bits stored in buffer i*3/2
      // and the 4 least significant bits in a the upper 4 bits of the byte after
      output_buffer[i*3/2]   = (val & 0x0FF0) >> 4;
      output_buffer[i*3/2+1] = (val & 0x000F) << 4; // this also clear the lower 4 bits
    } else {
      // off samples occupy the lower four bits (carefully not clearing the upper bits)
      // and the whole next byte
      output_buffer[i*3/2]  |= (val & 0x0F00) >> 8;
      output_buffer[i*3/2+1] = (val & 0x00FF);
    }
  }

  // wait until there is space
  /* Serial.println("samples acquired"); */
  /* while(Serial.availableForWrite() < sizeof(header) + sizeof(output_buffer)) { */
  /*   // stall */
  /*   delay(1); */
  /*   Serial.print("not enough space; can only send "); */
  /*   Serial.println(Serial.availableForWrite()); */
  /* } */

  /* // send header */
  Serial.write(header, sizeof(header));
  Serial.write(output_buffer, sizeof(output_buffer));

  //delay(100);
}

/* B */
/* 0 0.11-4 */
/* 1 0.3-0 + 1.11-8 */
/* 2 1.7-0 */

/* 3 2.11-4 */
/* 4 2.3-0 + 3.11-8 */
/* 5 3.7-0 */

/* 6 4.11-4 */
/* 7 4.3-0 + 5.11-8 */
/* 8 5.7-0 */
