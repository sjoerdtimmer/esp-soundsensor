#include <Arduino.h>
#include <driver/i2s.h>
#include "fft.h"
#include "window.h"
#include "wifi_portal.h"
#include "mqtt.h"
#include "credentials.h"


#define LOG2(X) ((unsigned) (8*sizeof (unsigned long long) - __builtin_clzll((X)) - 1))


#define SAMPLES (1024)       ///< at sample frequency of 22,627 kHz with 2048 samples, duration is 90 ms.
//#define SAMPLE_FREQ 22627          ///< this makes a bin bandwith of 22627 / 2048 = 11 Hz
//#define SAMPLE_FREQ 44100
#define SAMPLE_FREQ 20000
#define BANDS 25 // 2048 fft bins in an exponential band distribution
#define DCPEAK_BINS 2

// we first average a few fft's to get rid of noise
// and then we collect average and maximum over a longer period
#define AVERAGES 10
#define REPORT_PERIOD 5000 // (ms)

unsigned long int chipid = ESP.getEfuseMac();

const i2s_port_t I2S_PORT = I2S_NUM_0;

// The I2S config as per the example
const i2s_config_t i2s_config = {
  .mode                 = i2s_mode_t(I2S_MODE_MASTER | I2S_MODE_RX),  // Receive, not transfer
  .sample_rate          = SAMPLE_FREQ,
  .bits_per_sample      = I2S_BITS_PER_SAMPLE_32BIT,                  // only 24 bits are used
  .channel_format       = I2S_CHANNEL_FMT_ONLY_LEFT,                  // although the SEL config should be left, it seems to transmit on right, or not
  .communication_format = i2s_comm_format_t(I2S_COMM_FORMAT_I2S /*| I2S_COMM_FORMAT_I2S_MSB*/),
  .intr_alloc_flags     = ESP_INTR_FLAG_LEVEL1,                       // Interrupt level 1
  .dma_buf_count        = 4,                                          // number of buffers
  .dma_buf_len          = 1024,                                       //BLOCK_SIZE, samples per buffer
  .use_apll             = false,
  .tx_desc_auto_clear   = false,
  .fixed_mclk           = 0
};

const i2s_pin_config_t pin_config = {
  .bck_io_num   = 12,
  .ws_io_num    = 13,
  .data_out_num = -1,
  .data_in_num  = 35
};


//uint8_t header[] = {0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF};

bool audio_requested = false;
bool audio_ready = false;

int32_t samples[SAMPLES];
float * window;
float bands[BANDS];
float band_weight[BANDS];

float averaged[BANDS];
float maximums[BANDS];
int num_averages;
long int last_report = 0;

TaskHandle_t Task0;
void audio_loop(void*);
fft_config_t *real_fft_plan;



void setup() {
  // Initialize serial
  Serial.begin(576000);
  delay(100);

  // Create the FFT config structure
  real_fft_plan = fft_init(SAMPLES, FFT_REAL, FFT_FORWARD, NULL, NULL);
  window = blackmanharris(SAMPLES);

  // connect to wifi
  char * mqtt_server = setup_wifi_and_portal();

  // connect to mqtt
  setup_mqtt(mqtt_server);

  // pre-calculate weights
  for (int i=0; i<BANDS; i++) band_weight[i] = 0;
  for (int i=DCPEAK_BINS; i<SAMPLES/2; i++) {
    int band = i * (SAMPLES - i) * BANDS / (SAMPLES/2) / (SAMPLES/2);
    band_weight[band]++;
  }

  // prep reporting counters
  for (int i=0; i<BANDS; i++) {
    averaged[i] = 0.0;
    maximums[i] = 0.0;
  }

  last_report = millis();

  // Create a background task
  xTaskCreatePinnedToCore(
                          audio_loop,  /* Task function. */
                          "Task0",     /* name of task. */
                          40000,       /* Stack size of task */
                          NULL,        /* parameter of the task */
                          1,           /* priority of the task */
                          &Task0,      /* Task handle to keep track of created task */
                        0);          /* pin task to core 0 */
}




void audio_loop(void *) {
  // Configuring the I2S driver and pins.
  // This function must be called before any I2S driver read/write operations.
  esp_err_t _err = i2s_driver_install(I2S_PORT, &i2s_config, 0, NULL);
  if (_err != ESP_OK) {
    printf("Failed installing I2S driver: %d\n", _err);
    while (true);
  }

  _err = i2s_set_pin(I2S_PORT, &pin_config);
  if (_err != ESP_OK) {
    printf("Failed setting pin: %d\n", _err);
    while (true);
  }
  printf("I2S driver installed.\n");

  size_t num_bytes_read;

  //i2s_start(I2S_PORT);
  char internal_samples[SAMPLES * 4];

  while (true) {
    //printf("loop\n");
    esp_err_t _err = i2s_read(
                              I2S_PORT,
                              internal_samples,
                              SAMPLES * 4,        // 4 bytes per sample
                              &num_bytes_read,
                              portMAX_DELAY
                              );    // no timeout

    // TODO: do something with _err
    // TODO: check that all 4th bytes are 0x00
    // TODO: check num_bytes_read

    if (audio_requested) {
      //printf("audio request received\n");
      audio_requested = false;
      memcpy(samples, internal_samples, 4*SAMPLES);
      audio_ready = true;
    } // else: just discard
  }
}




void loop() {
  // process wifi and portal
  wifi_and_portal_update();

  // process mqtt events
  if (!mqtt_loop()) {
    // give up, no mqtt connection
    //
    return;
  }

  //printf("Wifi and MQTT good. Now getting audio...\n");


  // get a batch of data
  audio_requested = true;
  //printf("audio request sent\n");
  while (!audio_ready) {
    delay(1);
  }
  audio_ready = false;
  //printf("data received\n");

  // Fill array with data
  for (int k = 0 ; k < SAMPLES ; k++) {
    real_fft_plan->input[k] = (float)(samples[k] >> 8);// * window[k];
  }

  // Execute fft:
  fft_execute(real_fft_plan);

  // group into bands
  for (int i=0; i<BANDS; i++) {
    bands[i] = 0.0;
  }
  for (int i=DCPEAK_BINS; i<SAMPLES/2; i++) {
    float pow =
      real_fft_plan->output[2*i] * real_fft_plan->output[2*i] +
      real_fft_plan->output[2*i+1] * real_fft_plan->output[2*i+1];
    int band;

    // Method 1: Log-2 based. exponentially more bins per band
    //band = LOG2(i);     // the band index bin 0 is discarded, bin 1 goes in band 0, bins 2 and 3 go in band 1 and so forth
    //weight = 1 << band; // how many bins are in this band

    // Method 2: linear
    //band = i * BANDS / (SAMPLES / 2);
    //weight = 1;

    // Method 3: quadratic curve
    band = i * (SAMPLES - i) * BANDS / (SAMPLES/2) / (SAMPLES/2);
    bands[band] += pow / band_weight[band];
  }

  // make a log10 to get power
  for (int i=0; i<BANDS; i++) {
    bands[i] = log10(bands[i]);
  }


  // accumulate for report
  for (int i=0; i<BANDS; i++) {
    averaged[i] += bands[i];
    maximums[i] = max(bands[i], maximums[i]);
  }
  num_averages++;

  // check if a report is requested
  if (millis() - last_report > REPORT_PERIOD) {
    // print
    //Serial.write(header, sizeof(header));
    printf("Reporting...\n");

    // write band data:
    for (int i=0; i<BANDS; i++) {
      float val = averaged[i] / num_averages;
      //Serial.write((char*)&val, sizeof(val));
      char topic[64];

      sprintf(topic, "sensors/sound/%08lX/band/%d", chipid, i);
      mqtt_publish(topic, val);

    }

    // clear
    for (int i=0; i<BANDS; i++) {
      averaged[i] = 0.0;
      maximums[i] = 0.0;
    }
    num_averages = 0;

    // and remember the time
    last_report = millis();
  }



}
