#include <WiFiClient.h>
#include <PubSubClient.h>
#include "mqtt.h"

WiFiClient espClient;
PubSubClient client(espClient);
#define MQTT_MSG_SIZE (1024)
char mqtt_msg[MQTT_MSG_SIZE];
extern unsigned long int chipid;

void setup_mqtt(char * hostname) {
  client.setServer(hostname, 1883);
}

bool reconnect() {
  Serial.print("Attempting MQTT re-connection...");
  // Create a random client ID
  char clientId[6+8+1];
  sprintf(clientId, "ESP32-%08lX", chipid);
  // Attempt to connect
  if (client.connect(clientId)) {
    Serial.println("connected");
    return true;
  } else {
    Serial.print("failed, rc=");
    Serial.print(client.state());
    Serial.println(" try again in next loop");
    // Wait 5 seconds before retrying
    //delay(5000);
    return false;
  }
}

bool mqtt_loop() {
  // make sure mqtt is connected
  if (client.connected()) {
    client.loop();
    return true;
  } else {
    reconnect();
    return false;
  }
}

void mqtt_publish(char * topic, double value) {
  //snprintf(mqtt_msg, MQTT_MSG_SIZE, "%s %f", topic, value);
  char mqtt_val[64];
  sprintf(mqtt_val, "%.4f", value);
  //printf("publishing topic: '%s' value:'%s'\n", topic, mqtt_val);
  //client.publish(mqtt_msg, mqtt_val);
  client.publish(topic, mqtt_val);
}
