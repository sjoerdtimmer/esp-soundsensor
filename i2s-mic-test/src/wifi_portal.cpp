#include "wifi_portal.h"
#include <FS.h>
#include <SPIFFS.h>
#include <ArduinoJson.h>

// wifimanager can run in a blocking mode or a non blocking mode
// Be sure to know how to process loops with no delay() if using non blocking
// bool wm_nonblocking = false; // change to true to use non blocking

WiFiManager wm; // global wm instance

char mqtt_server[64];
bool should_save_config = false;

void saveConfigCallback() {
  printf("Saving mqtt server in json\n");
  should_save_config = true;
}

void read_config_json() {
  if (SPIFFS.begin(true)) {
    Serial.println("mounted file system");
    if (SPIFFS.exists("/config.json")) {
      //file exists, reading and loading
      Serial.println("reading config file");
      File configFile = SPIFFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonDocument json(1024);
        auto deserializeError = deserializeJson(json, buf.get());
        serializeJson(json, Serial);
        if ( ! deserializeError ) {
          Serial.println("\nparsed json");
          strcpy(mqtt_server, json["mqtt_server"]);
        } else {
          Serial.println("failed to load json config");
        }
        configFile.close();
      }
    }
  } else {
    Serial.println("failed to mount FS");
  }
}

char * setup_wifi_and_portal() {
  WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP
  pinMode(TRIGGER_PIN, INPUT);
  // wm.resetSettings(); // wipe settings
  // wm.setConfigPortalBlocking(false);

  // fix to work with some android devices
  wm.setAPStaticIPConfig(IPAddress(8,8,8,8), IPAddress(8,8,8,8), IPAddress(255,255,255,0)); // set static ip,gw,sn
  wm.setConfigPortalTimeout(30);
  wm.setAPClientCheck(true); // no timeout when >=1 client is connected to AP


  // get the previous mqtt server from json
  read_config_json();
  WiFiManagerParameter custom_mqtt_server("server", "mqtt server", mqtt_server, 64);
  wm.setSaveConfigCallback(saveConfigCallback);
  wm.addParameter(&custom_mqtt_server);

  // connect to wifi or start portal
  if (!wm.autoConnect("soundsensor")) {
    printf("Failed to connect or timeout.\n");
    ESP.restart();
  } else {
    printf("connected\n");
  }

  // now get the mqtt server from the portal
  strcpy(mqtt_server, custom_mqtt_server.getValue());
  Serial.println("\tmqtt_server : " + String(mqtt_server));

  if (should_save_config) {
    Serial.println("saving config");
    DynamicJsonDocument json(1024);
    json["mqtt_server"] = mqtt_server;
    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }
    serializeJson(json, Serial);
    serializeJson(json, configFile);

    return mqtt_server;
  }
}


bool _trigger_pin_prev_state = HIGH;
long int _trigger_pin_low_time;
void wifi_and_portal_update() {
  // wm.process(); // Only needed in non-blocking mode

  // remember at what time trigger pin went low
  int trigger = digitalRead(TRIGGER_PIN);
  long int now = millis();
  if (trigger == LOW && _trigger_pin_prev_state == HIGH) {
    _trigger_pin_low_time = now;
  }

  // when it goes high, do something
  long int duration = now - _trigger_pin_low_time;
  if (trigger == HIGH && _trigger_pin_prev_state == LOW) {
    if (duration > 10) { // skip really short blips
      // re-trigger our portal
      printf("Starting config portal\n");
      wm.setConfigPortalTimeout(120);
      if (!wm.startConfigPortal("soundsensor")) {
        printf("Config portal timed out\n");
      } else {
        printf("Config portal completed sucesfully");
      }
    }
  }

  // if held for more than 3 seconds, erase all
  if (duration > 3000 && trigger == LOW) {
    printf("Erasing config\n");
    delay(100);
    wm.resetSettings();
    ESP.restart();
    delay(1000);
  }

  // remember pin state
  _trigger_pin_prev_state = trigger;

}
