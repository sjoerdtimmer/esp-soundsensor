#!/bin/env python3
from pyqtgraph.graphicsItems.AxisItem import AxisItem
import serial
import sys
#import matplotlib.pyplot as plt
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
#import re
import numpy as np
from scipy.signal import get_window
import time
import struct

from rich.traceback import install
install(show_locals=True)

NUM_SAMPLES = 1024
SAMPLE_RATE = 48e3
header = b'\x00\xff\x00\xff\x00\xff\x00\xff'
BANDS = 25
HISTORY_LEN = 100

class Plotter():

    def __init__(self):
        self.app = QtGui.QApplication([])
        self.image_data = np.zeros([HISTORY_LEN, BANDS])
        self.plot = pg.image(self.image_data, title='Plot')
        self.plot.setLevels(min=0, max=15)
        self.buffer = b""

        self.ser = ser = serial.Serial('/dev/ttyUSB0', 576000, timeout=0)
        # discard whatever might be in the buffer
        ser.flushInput()
        ser.flushOutput()

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.refresh)
        self.timer.start(10)




    def refresh(self):
        # add get spectrums until there are none to add
        raw = self.get_trace()
        if raw is None:
            return None
        bands = self.unpack(raw)
        #print(np.log10(bands))
        self.image_data = np.roll(self.image_data, -1, axis=0)
        #bands = np.log10(bands)
        self.image_data[-1] = bands

        self.plot.setImage(self.image_data, autoRange=False)
        self.plot.setLevels(min=7, max=15)
        self.app.processEvents()



    def get_trace(self):
        self.buffer += self.ser.read_all() or b""

        # find the first header
        first = self.buffer.find(header)
        if first < 0:
            return None
        if first > 0:
            print("First header not at start of buffer")

        # find the second header
        second = self.buffer.find(header, first + 1)
        if second < 0:
            self.buffer = self.buffer[first:]
            return None

        # dt = time.time() - self.last
        # print(dt)
        # self.last = time.time()
        # cut the chuck we want:
        data = self.buffer[first+len(header):second]
        if len(data) != 4*BANDS: # power spectrum has half the bins
            print(f"unexpected packet size: {len(data)} != {int(4*BANDS)}")
            print(f'first: {first} second: {second}')
            #print(self.buffer)
            self.buffer = self.buffer[second:]
            return None

        # truncate buffer
        self.buffer = self.buffer[second:]

        return data
        # return list(data)

    def unpack(self, data):
        it =  struct.iter_unpack('<f', data)
        return [v[0] for v in it]


    def run(self):
        self.app.exec_()


if __name__=="__main__":
    m = Plotter()
    m.run()
