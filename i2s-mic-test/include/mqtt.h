#pragma once

void setup_mqtt(char * hostname);
bool reconnect();
bool mqtt_loop();
void mqtt_publish(char * topic, double value);
